import React from 'react';
import { css, StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
  roomSidebar: {
    color: '#fff',
    background: '#16a085',
  },

  header: {
    padding: '20px 15px',
    marginBottom: '10px',
    width: '220px',
  },

  roomName: {
    marginBottom: '0',
    fontSize: '22px',
    lineHeight: '1',
    color: '#fff',
  },

  userList: {
    paddingLeft: '15px',
    listStyle: 'none',
  },

  username: {
    position: 'relative',
    paddingLeft: '20px',
    fontSize: '14px',
    fontWeight: '300',
    ':after': {
      position: 'absolute',
      top: '7px',
      left: '0',
      width: '8px',
      height: '8px',
      borderRadius: '50%',
      background: '#555',
      content: '""',
    },
  },

  listHeading: {
    marginLeft: '15px',
    marginBottom: '5px',
    fontSize: '13px',
    textTransform: 'uppercase',
  },
});

type User = {
  id: number,
  username: string,
}

type Props = {
  room: {
    name: string,
  },
  currentUser: {
    username: string,
  },
  presentUsers: Array<User>,
}

const RoomSidebar = ({ room, currentUser, presentUsers }: Props) =>
  <div className={css(styles.roomSidebar)}>
    <div className={css(styles.header)}>
      <h2 className={css(styles.roomName)}>{room.name}</h2>
      <div style={{ fontSize: '13px' }}>{currentUser.username}</div>
      <hr/>
    </div>
    <div className={css(styles.listHeading)}><b>Active Users</b></div>
    <ul className={css(styles.userList)}>
      {presentUsers.map(user =>
        <li key={user.id} className={css(styles.username)}>{user.username}</li>
      )}
    </ul>
  </div>;

export default RoomSidebar;