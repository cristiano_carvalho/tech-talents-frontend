import React from 'react';
import { Link } from 'react-router';
import { css, StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
  navbar: {
    textAlign: 'center',
    padding: '0 1rem',
    background: '#16a085',
    backgroundColor: '#16a085',
    boxShadow: '0 7px 7px -7px #333',
    marginBottom: '30px'
  },

  link: {
    color: '#fff',
    fontSize: '22px',
    fontWeight: 'bold',
    ':hover': {
      textDecoration: 'none',
    },
    ':focus': {
      textDecoration: 'none',
    },
  },

  logo: {
    padding: '10px',
    marginLeft: '40px'
  }
});

const Navbar = () =>
  <nav className={css(styles.navbar)}>
    <Link to="/" className={css(styles.link)}><img width="150" role="presentation" className={css(styles.logo)} src={require('../../img/logo.png')} /></Link>
  </nav>;

export default Navbar;
