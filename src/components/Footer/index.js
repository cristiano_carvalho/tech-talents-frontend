import React, { Component } from 'react';
import { css, StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
  footer: {
    textAlign: 'center',
    fontSize: '14px',
    color: '#fff'
  }
});

class Footer extends Component {
  render() {
    return (
      <footer className={css(styles.footer)}>
        <hr/>
        <p><strong>Tech Talents</strong> - Chat rooms for IT jobs.</p>
      </footer>
    )
  }
};

export default Footer;