import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router';
import { css, StyleSheet } from 'aphrodite';
import Input from '../Input';
import Typist from 'react-typist';

const styles = StyleSheet.create({
  card: {
    maxWidth: '315px',
    padding: '1.5rem',
    margin: '1rem auto',
    boxShadow: '0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)',
    borderRadius: '0px'
  },
});

type Props = {
  onSubmit: () => void,
  handleSubmit: () => void,
  submitting: boolean,
}

class LoginForm extends Component {
  props: Props

  handleSubmit = data => this.props.onSubmit(data);

  render() {
    const { handleSubmit, submitting } = this.props;

    return (
      <form
        className={`card ${css(styles.card)}`}
        onSubmit={handleSubmit(this.handleSubmit)}
      > 
        <div style={{ textAlign: 'center'}}><img role="presentation" src={require('../../img/user.png')}/></div>
        <div style={{ marginBottom: '2rem', textAlign: 'center', fontSize: '20px' }}>
            <Typist cursor={{ hideWhenDone: true }}>|> sudo su</Typist>
        </div>
        <Field name="email" type="text" component={Input} placeholder="Email" />
        <Field name="password" type="password" component={Input} placeholder="Password" />
        <button type="submit" disabled={submitting} className="btn btn-block btn-primary" style={{ backgroundColor: '#16a085', borderColor: '#16a085'}}>
          {submitting ? 'Accessing...' : 'Access your account'}
        </button>
        <hr style={{ margin: '2rem 0' }} />
        <Link to="/signup" className="btn btn-block btn-secondary">
          Sign Up
        </Link>
      </form>
    );
  }
}

const validate = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Required field';
  }
  if (!values.password) {
    errors.password = 'Required field';
  }
  return errors;
};

export default reduxForm({
  form: 'login',
  validate,
})(LoginForm);
