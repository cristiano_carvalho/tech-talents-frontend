import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { signup } from '../../actions/session';
import SignUpForm from '../../components/SignUpForm';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';

type Props = {
  signup: () => void,
}

class SignUp extends Component {
  static contextTypes = {
    route: PropTypes.object,
  }

  props: Props

  handleSignUp = data => this.props.signup(data, this.context.router);

  render() {
    return (
      <div style={{ flex: '1'}}>
        <Navbar />
        <SignUpForm onSubmit={this.handleSignUp} />
        <Footer />
      </div>
    );
  }
}

export default connect(null, {signup})(SignUp);